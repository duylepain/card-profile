/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      boxShadow: {
        'custom1': 'rgba(149, 157, 165, 0.2) 0px 8px 24px;'
      }
    },
  },
  plugins: [],
}
