import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  title = 'card-profile';
  darkModeCheck:boolean = false;

  darkMode(){
    console.log('OK');
    this.darkModeCheck = true;
    setTimeout(()=>{
      this.darkModeCheck = false;
    },2000);
  }
}
